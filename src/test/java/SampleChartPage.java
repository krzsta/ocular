import com.testautomationguru.ocular.Ocular;
import com.testautomationguru.ocular.comparator.OcularResult;
import com.testautomationguru.ocular.snapshot.Snap;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

@Snap
public class SampleChartPage {
    private WebDriver driver;
    private Map<String, String> map;

    @FindBy(id = "ui-id-2")
    public WebElement systemSettings;

    @FindBy(css = "#login > div.top > div")
    private WebElement iIcon;

    @FindBy(xpath = "//*[@id=\"wrapper\"]/main/app-login/div/iframe")
    private WebElement frame;

    @FindBy(id = "loginFields")
    private WebElement loginFields;

    public SampleChartPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

        map = new HashMap<>();
        map.put("loginFields", "loginFields.png");
    }

    public boolean verifyBuyersChart() {
        return this.verifyChart(map.get("loginFields"), loginFields);
    }

    public boolean verifyChart(String fileName, WebElement element) {

        Path path = Paths.get(fileName);

        OcularResult result = Ocular
                .snapshot()
                .from(path)
                .sample()
                .using(driver)
                .element(element)
                .compare();

        return result.isEqualsImages();
    }

    public void switchToIframe() {
        driver.switchTo().frame(frame);
    }

    public void hover() {
        Actions action = new Actions(driver);
        action.moveToElement(iIcon).build().perform();
    }
}