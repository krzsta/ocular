import com.testautomationguru.ocular.Ocular;
import com.testautomationguru.ocular.snapshot.Snap;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

@Snap()
public class OcularDemo {
    private WebDriver driver;
    private SampleChartPage page;
    private String linuxChromeDriverPath = "./src/main/resources/drivers/chromedriver";

    @BeforeTest
    public void ocularConfig() {
        Ocular.config()
                .snapshotPath(Paths.get("./src/main/resources/snap"))
                .resultPath(Paths.get("./src/main/resources/result"))
                .globalSimilarity(95);
    }

    @BeforeMethod
    public void beforeMethod() {
        System.setProperty("webdriver.chrome.driver", linuxChromeDriverPath);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void afterMethod() {
        driver.quit();
    }

    @Test
    public void baselineTest() throws InterruptedException {
        driver.get("https://online.gbox.pl/login");

        page = new SampleChartPage(driver);
        page.switchToIframe();

        Assert.assertTrue(page.verifyBuyersChart());
    }

    @Test
    public void baselineTest2() throws InterruptedException {
        driver.get("https://online.gbox.pl/login");

        page = new SampleChartPage(driver);
        page.switchToIframe();
        page.hover();

        Assert.assertTrue(page.verifyChart("hover.png", page.systemSettings));
    }
}
